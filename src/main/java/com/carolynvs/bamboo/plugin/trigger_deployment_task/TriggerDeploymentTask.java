package com.carolynvs.bamboo.plugin.trigger_deployment_task;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.deployments.execution.actions.ExecuteManualDeployment;
import com.atlassian.bamboo.deployments.execution.triggering.EnvironmentTriggeringActionFactory;
import com.atlassian.bamboo.task.*;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class TriggerDeploymentTask implements TaskType
{
    public static final String ENVIRONMENT_ID = "deploymentId";
    private final ExecuteManualDeployment manualDeployment;
    private final EnvironmentTriggeringActionFactory environmentTriggeringActionFactory;

    public TriggerDeploymentTask(ExecuteManualDeployment manualDeployment, EnvironmentTriggeringActionFactory environmentTriggeringActionFactory)
    {
        this.manualDeployment = manualDeployment;
        this.environmentTriggeringActionFactory = environmentTriggeringActionFactory;
    }

    @NotNull
    @Override
    public TaskResult execute(@NotNull TaskContext taskContext)
            throws TaskException
    {
        final BuildLogger buildLogger = taskContext.getBuildLogger();

        final long environmentId = Long.valueOf(taskContext.getConfigurationMap().get(ENVIRONMENT_ID));

        buildLogger.addBuildLogEntry(String.format("Triggering Deployment Project: %s", environmentId));

        manualDeployment.setEnvironmentId(environmentId);
        manualDeployment.setNewReleaseBuildResult(taskContext.getBuildContext().getBuildResultKey());
        manualDeployment.setEnvironmentTriggeringActionFactory(environmentTriggeringActionFactory);

        try {
            buildLogger.addBuildLogEntry("Preparing manual deployment");
            manualDeployment.prepare();
            manualDeployment.setActionErrors(new ArrayList<String>());
            buildLogger.addBuildLogEntry("Executing manual deployment");
            manualDeployment.execute();
        } catch (Exception e) {
            buildLogger.addErrorLogEntry(e.getMessage(), e);

            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build();
        }
        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }
}

